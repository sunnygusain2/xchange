# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Category.create!([
{ name:"community",     url: "community"},
{ name:"activities",     url: "activiies"},
{ name:"artists",     url: "artists"},
{ name:"childcare",     url: "childcare"},
{ name:"classes",     url: "classes"},
{ name:"events",     url: "events"},
{ name:"general",     url: "general"},
{ name:"groups",     url: "groups"},
{ name:"local news",     url: "local news"},
{ name:"lost+found",     url: "lost+found"},
{ name:"missed connections",     url: "missed connections"},
{ name:"musicians",     url: "musicians"},
{ name:"pets",     url: "pets"},
{ name:"politics",     url: "politics"},
{ name:"rants & raves",     url: "rants &raves"},
{ name:"rideshare",     url: "rideshare"},
{ name:"volunteers",     url: "volunteers"}

])
